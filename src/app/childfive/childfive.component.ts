import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-childfive',
  templateUrl: './childfive.component.html',
  styleUrls: ['./childfive.component.css']
})
export class ChildfiveComponent implements OnInit {

  @Input() childfiveName!: string;
  @Output() parentMessage = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    console.log(this.childfiveName);
  }

  sayHi() {
    this.parentMessage.emit('Hi great-great-great-grandparent (@Output())');
  }

}
