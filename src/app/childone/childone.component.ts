import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-childone',
  templateUrl: './childone.component.html',
  styleUrls: ['./childone.component.css']
})
export class ChildoneComponent implements OnInit {
  //messageFromChild: string = '';

  @Input() childoneName!: string;
  @Output() parentMessage = new EventEmitter<string>();
  

  constructor() { }

  ngOnInit(): void {
    //console.log(this.childoneName);
  }

  getGreeting(message: string) {
    this.parentMessage.emit(message);
    //this.messageFromChild = message;
  }  

}
